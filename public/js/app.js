'use strict';

var ocdApp = angular.module('ocdApp', [
	'ngRoute',
	'ocdControllers'		
]);

ocdApp.config(['$routeProvider', 
	function($routeProvider) {
		$routeProvider.when('/', {
			templateUrl: 'views/partials/home.html',
			controller: 'MainCtrl'
		})
		.when('/portfolio', {
			templateUrl: 'views/partials/portfolio.html',
			controller: 'MainCtrl'
		})
		.when('/services', {
			templateUrl: 'views/partials/services.html',
			controller: 'MainCtrl'
		})
		.when('/contact', {
			templateUrl: 'views/partials/contact.html',
			controller: 'MainCtrl'
		})
		.otherwise({
			redirectTo: '/'
		});
}])