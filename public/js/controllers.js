'use strict';

var ocdControllers = angular.module('ocdControllers', []);

ocdControllers.controller('MainCtrl', ['$scope', 
    function($scope) {
        $scope.welcome = 'Welcome to OCDetailing Automotive!';   
}]);