// app/routes.js

// grab the nerd model we just created
// var Nerd = require('./models/nerd');

module.exports = function(app) {

   // server routes ===========================================================
   // handle things like api calls
   // authentication routes

   // frontend routes =========================================================
   // route to handle all angular requests
   // all routes leads to the index, where angular's routing will take over
   app.get('*', function(req, res) {
      res.sendfile('./public/views/index.html'); // load our public/index.html file
   });

};
