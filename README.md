# README #

OCDetailing Wesbite Info and Setup (WIP)

### Overview ###

* Build/Deployment (deployed website @ ocdetailing.herokuapp.com/)
* Setup

### Build/Deployment ###

* There are 3 main components in the build and deployment cycle.
* Bitbucket, wercker, Heroku.
* Basically you make some changes to the source (./public), make
a commit and push to Bitbucket, then wercker will pick this up
and attempt to run the build process (check ./wercker.yml), finally
deploying it to Heroku.

### Setup ###

* You will need node.js installed.
* Clone this repo to your desired directory.
* Run npm install, this should install any required dependencies for node. (Refer to ./package.json)
* To install or update the front-end dependencies (if not in ./public/vendor) you can run Bower install.
(You will need Bower installed, just run npm install -g bower)
* When you make any changes it's pretty simple, just commit and push to the repo, it will then get
passed onto wercker, which will run a bunch of setup commands. If it passes it should be deployed to
Heroku successfully (most of the time).
* wercker status: 

     [![wercker status](https://app.wercker.com/status/0bed6ee8f8f5abae3fd0e1afc4b78595/m "wercker status")](https://app.wercker.com/project/bykey/0bed6ee8f8f5abae3fd0e1afc4b78595)

### Who do I talk to? ###

* Muhamed Delic (Solo developer for this project, shoot me an email @ muhamed.delicc@live.com)